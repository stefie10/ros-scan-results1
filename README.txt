README
ROS scan results
N. DeMarinis

This directory contains exported results from our ROS scan data.  This
document describes the directory structure and purpose of each file:

The root directory contains the following results files, as well as a
directory with results specific to each scan:
 - result_summary.tsv:  Counts of hosts in various overlapping
   categories for each scan.  View by opening in excel, or using
   something like column -t.
   For more information on "needs_more_info", see Notes.
 - query_list.txt:  Explanation of queries currently being performed
 - all_topics_complete.json:  List of all topics returned by all hosts,
   with references to the IP addresses responding to each topic
 - all_topics_level1.json:  Same as above, except we only consider the
   first "level" of a topic paramter.  So, if one host provides
   "/camera/foo" and another provides "/camera/bar", we count them
   both as the same
 - all_topics_tree.json:  A tree-representation of all topics.  For
   each level in the structure "separated by the /", we report the
   hosts that listed the topic.  You can think of this as the most extended
   form of the "level1" output
 - match_summary.json: A summary of attempts to combine hosts by
   machine name, hostname, or topic data.  In addition to general
   statistics (count of hosts in the grouped category), this file
   lists each group of hosts that were matched and combined to allow
   inspection.


Each scan directory contains the following:
 - host_summary.tsv:  Summary table of results (IP, GeoIP parameters,
 etc.), suitable for importing into Excel or another spreadsheet
 program (or just column -t)

 - search_data.json: Results of simple keyword searches for various
 topics and paramters, listing hosts that matched on each search.
 Once we figure out what to search, this will get more interesting.

 - search_summary.tsv: Table-formatted summary of above search results
 (view with something like column -t)

 - hosts/:  Directory with one file for each IP that responded to the
 scan, including all of their JSON results.  This file includes all of
 the data we have collected for a given host

 - by_class/: Directory with subdirectories for each classification of
 host (robot, simulator, etc.).  Each class directory contains a
 symlink for each host in that category.

 - descriptions/:  robot_description.xml files collected from each
 host that provided one, named by the host's IP address

 - description_name_summary.tsv: Summary or names reported by
 robot_description.xml files, their IP addresses for more information,
 and their status for the gazebo search query.  More interesting
 information can be found in host_summary.tsv.

Other notes:
 - I have recently discovered the tool "jq" for parsing JSON files in
 bash scripts, which is quite useful.
 - If you would like any of this data presented differently, please
 let me know.


**Performing manual classifications**

All manual tagging can be performed by editing hosts.json in the root
directory of this repo.  The file contains a section for each scan and
its responding hosts.  To tag hosts manually:

 - Add ONE of the following tags to the manual_tags field will
 classify the host in the given category on the next run:
    Tag	              Category
    "robot":          CLASS_ROBOT,
    "sim":            CLASS_SIM,
    "empty":          CLASS_EMPTY,
    "only_sensors":   CLASS_ONLY_SENSORS,
    "only_actuators": CLASS_ONLY_ACTUATORS,
    "only_services":  CLASS_SVC,
    "other":          CLASS_UNCLASSIFIED,
 - The "description"" field is intended a comment and is not parsed in
 any way for now. 
