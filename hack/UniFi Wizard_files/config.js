window.unifiConfig = {
  UnifiAPILocalPath: '/data/dev/api',
  UnifiAPIBasePath: '/api',
  UnifiAPISelfPath: '/api/self',
  UnifiAPIStatusPath: '/status',
  UnifiFileBasePath: '',
  UnifiGuestBasePath: '/guest',
  UnifiPrintBasePath: '/print',
  UnifiHotspotHost: '',
  UnifiWsHost: '',
  CloudDashboardHost: 'https://unifi.ubnt.com',
  UnifiCloudHost: 'device-airos.svc.ubnt.com',
  UnifiDevicesEndpoint: '/api/airos/v1/unifi/devices/',
  TurnCredentialsApiEndpoint: '/api/airos/v1/unifi/turn/creds?username=',
  SSOAPIHostname: 'sso.ubnt.com',
  SSOAPIEndpoint: '/api/sso/v1/user/self',
  SsoRegisterUrl: 'https://account.ubnt.com/register',
  AccountHostname: 'account.ubnt.com',
  CsrfTokenUrlCheckEnabled: true,
  WebSocketEnabled: true
};

window.unifiConstant = {
  GoogleMapsApiKey: 'AIzaSyDW1OnFXY2kzpH77F7fEB66WlkFRtKX3p4',
  GoogleAnalyticsCloudKey: 'UA-2432820-22',
  GoogleAnalyticsDemoKey: 'UA-2432820-34',
  VERSION: '5.6.22.0',
  ShopifyApiKey: '7dbfb8a49f454a553d4a17b8d22b0970',
  ShopifyAppId: '8',
  ShopifyDomain: 'store.ubnt.com'
};

window.webrtcConfig = {
  SignallingChannelHttpsEndpoint: '/api/airos/v1/unifi/devices/request/',
  SignallingChannelHttpsHost: 'https://device-airos.svc.ubnt.com',
  SignallingChannelWsEndpoint: '/api/airos/v1/unifi/events',
  SignallingChannelWsHost: 'wss://device-airos.svc.ubnt.com',
  SignallingChannelType: 'WS',
  WebRtcStartAsCaller: false,
  WebRtcLegacyEngine: false,
  UseWebrtc: false,
  VersionCheckEnabled: true
};

window.unifiSharedConstant = {
  DISCOVERY_TOOL_KEY: 'hmpigflbjeapnknladcfphgkemopofig'
};
