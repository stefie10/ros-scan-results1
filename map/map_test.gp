set terminal postscript eps size 6.5,4.0 color
set output 'map_test.eps'

unset key

set format ''
set tics scale 0
stats 'world_110m.txt'

set xrange [-180:180]
set yrange [-85:85]

set key left center
set key vertical
set key box width 0.8
set key inside
set key spacing 1.0 samplen 0.8 font ",18"

plot 'world_110m.txt' with lines title "" linestyle 1 lc "#000000",\
     './has_camera.txt'   with points title "Camera"   pointsize 1 pointtype 2  lw 2 lt rgb "#ff0000",\
     './has_actuator.txt' with points title "Actuator" pointsize 1 pointtype 5  lw 2 lt rgb "#0000ff",\
     './other.txt'        with points title "Other Instances"    pointsize 1 pointtype 13 lw 2 lt rgb "#00ff00"
