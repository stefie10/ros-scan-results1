# search_query_list.py

import itertools

from search_query import SQuery
from search_query import SearchTerm
from search_query import SAnd
from search_query import SNot
from search_query import SOr

import search_utils as su

from search_info import SEARCH_PARAM
from search_info import SEARCH_TOPIC
from search_info import SEARCH_HOST
#from search_info import SEARCH_ALL
from search_info import SEARCH_HAS_SUBSCRIBERS


# def SQAll(name, *params):
#     return SQuery(name, SearchTerm(SEARCH_PARAM, *params))


# def HasTerms(*topics):
#     return [SearchTerm(SEARCH_PARAM, t) for t in topics]


def HasTerm(*params):
    return SearchTerm(SEARCH_TOPIC, *params)


def HasSubscriber(*params):
    return SearchTerm(SEARCH_HAS_SUBSCRIBERS, *params)


def HasNoTerm(*params):
    return SNot(SearchTerm(SEARCH_TOPIC, *params))


IS_NOT_SIM = SAnd(HasNoTerm("gazebo"),
                  HasNoTerm("stageros"),
                  HasNoTerm("simulator"),
                  HasNoTerm("use_sim_time"),
                  HasNoTerm("torcs_ros"),
                  HasNoTerm("unity"),
                  HasNoTerm("turtlesim"),
                  HasNoTerm("dreamview"),
                  HasNoTerm("videofile"),
                  HasNoTerm("uwsim"),
                  HasNoTerm("file_player"))


# def HasNoSim(*params):
#     return SAnd(SearchTerm(SEARCH_PARAM, *params), IS_NOT_SIM)


# Shorthand for a query (which is not printed in the table)
def QNS(name, expr):
    #return SQuery(name, SAnd(IS_NOT_SIM, expr))
    return SQuery(name, expr,
                  print_query=False, printable_name=name)


# Printable query that is added to the table
def QNP(name, printable_name, expr):
    return SQuery(name, expr,
                  print_query=True, printable_name=printable_name)


SEARCH_QUERIES = {
    "control": [
        # Control:  Everything should have a rosversion parameter
        SQuery("param_rosversion", HasTerm("rosversion")),
    ],
    "simulators": [
        # ########## SIMULATORS ##########
#        SQuery("sim_sim", HasTerm("sim")),
        SQuery("sim_fake", HasTerm("fake")),

        QNS("use_sim_time",
            HasTerm("use_sim_time")),

        SQuery("sim_any", SNot(IS_NOT_SIM)),

        SQuery("sim_gazebo", HasTerm("gazebo"),
               print_query=True, printable_name="Gazebo"),

        SQuery("unity", HasTerm("unity"),
               print_query=True, printable_name="Unity"),

        SQuery("stageros", HasTerm("stageros"),
               print_query=True, printable_name="Stageros"),

        SQuery("torcs_ros", HasTerm("torcs_ros"),
               print_query=True, printable_name="torcs_ros"),

        SQuery("dreamview", HasTerm("dreamview"),
               print_query=True, printable_name="Dreamview"),

        SQuery("file_player", HasTerm("file_player"),
               print_query=True, printable_name="file_player"),

        SQuery("rosbag_player", HasTerm("rosbag_player"),
               print_query=True, printable_name="rosbag_player"),

        SQuery("kitti_player", HasTerm("kitti_player"),
               print_query=True, printable_name="kitti_player"),


        QNS("turtlesim",
            HasTerm("turtlesim")),
    ],

    # ########## SENSORS ##########
    "sensors": [
        # Camera
        #QNS("camera_all", SOr(HasTerm("camera"), HasTerm("image"))),

        QNP("camera_nofake", "Camera",
            SOr(HasTerm("camera", "!fake"),
                HasTerm("image", "!fake"),
                HasTerm("pointgrey", "!fake"),
                HasTerm("usb_cam", "!fake"))),
        QNP("camera_depth", "\tCamera + Depth",
            SOr(HasTerm("camera", "depth", "!fake"),
                HasTerm("compressedDepth"),
                HasTerm("registered_points"))),
        QNP("camera_rgb", "\tCamera + RGB",
            SOr(HasTerm("camera", "rgb", "!fake"),
                HasTerm("kinect2"))),
        QNP("camera_stereo", "\tCamera + Stereo",
            SOr(HasTerm("camera", "stereo", "!fake"),
                HasTerm("image", "stereo", "!fake"))),

        QNP("kinect", "Kinect",
            HasTerm("kinect")),

        QNP("imu", "IMU",
            HasTerm("imu")),
        QNP("gyro", "Gyro",
            HasTerm("gyro")),

        QNP("lidar", "Lidar",
            SOr(HasTerm("lidar"),
                HasTerm("hokuyo"),
                HasTerm("scan"),
                HasTerm("lms511"),
                HasTerm("laser_points_pc2"))),

        QNP("motion_capture", "Motion Capture",
            SOr(HasTerm("motion_capture"),
                HasTerm("mocap"),
                HasTerm("vicon"))),

        # QNP("barometer", "Barometer",
        #     SOr(HasTerm("baro"), HasTerm("barometer"))),
        QNP("compass", "Compass",
            HasTerm("compass")),

        QNP("odometry", "Odometry",
            HasTerm("odom")),
        QNP("pressure", "Pressure",
            SOr(HasTerm("altimeter"),
                HasTerm("barometer"),
                HasTerm("baro"),
                HasTerm("altimer_data"))),

        QNP("bumper", "Contact",
            SOr(HasTerm("bumper"),
                HasTerm("contact"))),

        QNP("biotac", "biotac",
            HasTerm("biotac")),

        QNP("velodyne", "Velodyne",
            SOr(HasTerm("velodyne"),
                HasTerm("velodyne64"))),

        QNP("point_cloud", "point_cloud",
            SOr(HasTerm("point_cloud"),
                HasTerm("depth_map"))),

        QNP("force", "Force",
            HasTerm("ForceSensor")),

        QNP("radar", "Radar",
            HasTerm("radar")),

        QNP("gps_nofake", "Geolocation",
            SOr(HasTerm("gps", "!fake", "!sim"),
                HasTerm("gnss", "!fake", "!sim"),
                HasTerm("vectornav", "!fake", "!sim"))),
        # QNS("gps_all", SOr(HasTerm("gps"),
        #                    HasTerm("vectornav"),
        #                    HasTerm("gnss"))),

        QNP("audio", "Audio",
            HasTerm("microphone")),

        QNP("temperature", "Temperature",
            SOr(HasTerm("bsc201_master"),
                HasTerm("temperature", "!planning"))),

        QNP("battery", "Battery Monitor",
            SOr(HasTerm("battery"),
                HasTerm("voltage"))),

        QNP("print_sensor", "Printhead status",
            HasTerm("PrinterPosition")),

        QNP("joystick", "Joystick",
            SOr(HasTerm("joy"),
                HasTerm("joystick"))),

        # QNP("teleop_keyboard", "teleop_keyboard",
        #     HasTerm("teleop_keyboard")),

    ],
    "actuators": [
        # ########## ACTUATORS ###########
        QNP("cmd_vel", "Movable base",
            HasTerm("cmd_vel")),

        QNP("servo", "Servo",
            SOr(HasTerm("servo"),
                HasTerm("set_position"),
                HasTerm("set_jaw_position"))),

        QNP("lights", "Lights",
            SOr(HasTerm("set_light"),
                HasTerm("set_lights"),
                HasTerm("set_red_level"))),

        QNP("arm_topics", "Arm",
            SOr(HasTerm("joint_trajectory"),
                HasTerm("schunk_neck"),
                HasTerm("pose_action"),
                HasTerm("trajectory_controller", "joint"),
                HasTerm("action_controller", "joint"))),

        QNP("gripper", "Gripper",
            SOr(HasTerm("gripper"),
                HasTerm("fingers_action"))),

        QNP("flipper", "Flippers", HasTerm("flipper")),

        QNP("sound_play", "Sound",
            HasTerm("sound_play")),

        QNP("heartbeat", "Heartbeat",
            SOr(HasTerm("heart"),
                HasTerm("heartbeat"))),

        QNP("voice", "Voice",
            SOr(HasTerm("voice"),
                HasTerm("/say/goal"),
                HasTerm("speech"))),

        QNP("velocity", "Velocity",
            HasTerm("motVel")),

        QNP("move_base_simple", "move_base_simple",
            HasSubscriber("move_base_simple")),

        QNP("twist_cmd", "twist_cmd",
            HasTerm("twist_cmd")),
        QNP("steer_cmd", "steer_cmd",
            HasTerm("steer_cmd")),
        QNP("brake_cmd", "brake_cmd",
            HasTerm("brake_cmd")),

        # Specific robots
        QNS("command_joint_position", HasTerm("command_joint_position")),

        QNP("MotorCommand", "MotorCommand",
            HasTerm("MotorCommand")),
        QNP("inceptor_command", "inceptor_command",
            HasTerm("inceptor_command")),
        QNP("flystate2phidgetsanalog", "flystate2phidgetsanalog",
            HasTerm("flystate2phidgetsanalog")),

        QNS("SendJointTrajectory", HasTerm("SendJointTrajectory")),

        # QNS("joint_states", HasTerm("joint_states")),
        # QNS("move_base_sample", HasTerm("move_base_sample/goal")),

        QNP("e_stop", "Emergency Stop",
            SOr(HasTerm("e_stop"),
                HasTerm("emergency_stop"))),

        QNP("print_head", "Printhead",
            HasTerm("PrinterCommand")),

        QNS("yaw_goal", HasTerm("yaw_goal")),
        QNS("roll_goal", HasTerm("roll_goal")),
        QNS("velocity_commands", HasTerm("velocity_commands")),
        QNS("follow_command", HasTerm("follow_command")),
        QNS("set_movement", HasTerm("set_movement")),

    ],
    "other": [
        # ########## OTHER ELEMENTS ###########
        QNP("rosbridge", "Rosbridge", HasTerm("rosbridge")),
        SQuery("rviz", HasTerm("rviz"),
               print_query=True, printable_name="RViz"),

        QNP("moveit", "MoveIt!",
            SAnd(HasTerm("moveit"), HasTerm("move_group"))),

        QNP("openrave", "OpenRAVE",
            HasTerm("openrave")),

        QNP("transform", "Transform Library (tf)",
            SOr(HasTerm("tf"), HasTerm("tf_static"))),

        QNP("fiducial", "Fiducial Libraries",
            SOr(HasTerm("apriltag"),
                HasTerm("apriltags"),
                HasTerm("av_track_avlar"))),

        QNP("ros_tutorials", "ROS Tutorials",
            SOr(HasTerm("add_two_ints"),
                HasTerm("turtlesim"))),

        QNP("master_discovery", "master_discovery",
            HasTerm("master_discovery")),
        QNP("master_sync", "master_sync",
            HasTerm("master_sync")),
        QNP("robot_position", "robot_position",
            HasTerm("robot_position")),
        QNP("robot_position", "robot_position",
            HasTerm("robot_position")),
        QNP("web_video_server", "web_video_server",
            HasTerm("web_video_server")),

    ],
    "robot_types": [
        # ########## ROBOT TYPES ###########
        QNS("baxter_all", HasTerm("baxter")),
        QNP("baxter_topics", "Baxter",
            SAnd(HasTerm("robot/analog_io"), HasTerm("robot/digital_io"),
                 HasTerm("left_hand_camera"), HasTerm("right_hand_camera"))),

        QNP("pr2", "PR2",
            HasTerm("pr2")),

        QNP("wam", "WAM",
            HasTerm("wam")),

        QNP("jaco", "JACO",
            HasTerm("j2n6s300")),

        QNP("turtlebot_all", "Turtlebot",
            HasTerm("turtlebot")),

        QNS("turtlebot_nosim", SAnd(HasTerm("turtlebot"),
                                    SNot(HasTerm("turtlesim")))),

        QNS("abb",
            HasTerm("abb")),

        QNP("davinci", "DaVinci",
            SOr(HasTerm("davinci"),
                HasTerm("dvrk"))),
    ],
    "targets": [
        QNS("car", HasTerm("car")),
        QNS("boat", HasTerm("boat")),
        QNS("drone", HasTerm("drone")),
        QNS("vehicle", HasTerm("vehicle")),
        QNS("uav", HasTerm("uav")),
        QNS("mav", HasTerm("mav")),

        QNS("flight", SOr(HasTerm("flight"),
                          HasTerm("fly"))),
        QNS("explore", HasTerm("explore")),
        QNS("planning", HasTerm("planning")),
        QNS("collision", HasTerm("collision")),
        QNS("map", HasTerm("map")),

        QNS("android", HasTerm("android")),

        QNS("credentials",
            SOr(HasTerm("password"),
                HasTerm("passwd"),
                HasTerm("secret"),
                HasTerm("privkey"))),

        QNS("firmware",
            HasTerm("firmware")),

        QNS("neck", HasTerm("neck")),
        QNS("head", HasTerm("head")),
        QNS("leg", HasTerm("leg")),
        QNS("torso", HasTerm("torso")),
        QNS("arm", HasTerm("arm")),


    ]
}

QUERY_NAMES = [query.name for query in itertools.chain.from_iterable(SEARCH_QUERIES.values())]
QUERY_TYPES = list(SEARCH_QUERIES.keys())
QUERY_TYPE_REPORT_LIST = ["sensors", "actuators", "simulators", "robot_types", "other", "targets"]

QUERY_TYPE_PRINTABLE_NAMES = {
    "sensors": "Sensors",
    "actuators": "Actuators",
    "simulators": "Simulators",
    "robot_types": "Robot Types",
    "other": "Libraries",
    "targets": "Various target applications",
}


def build_query_name_map():
    query_name_map = {}
    total_count = 0

    for query_type in QUERY_TYPES:
        type_count = 0
        queries = SEARCH_QUERIES[query_type]

        for query in queries:
            q_entry = {
                "query": query,
                "type": query_type,
                "index": total_count,
                "type_index": type_count,
            }
            query_name_map[query.name] = q_entry

    return query_name_map


QUERY_NAME_MAP = build_query_name_map()
